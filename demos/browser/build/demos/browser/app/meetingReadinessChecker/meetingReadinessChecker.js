"use strict";
// Copyright 2020 Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DemoMeetingApp = void 0;
require("../../style.scss");
require("bootstrap");
var index_1 = require("../../../../src/index");
var uuidv4 = require('uuid').v4;
var DemoMeetingApp = /** @class */ (function () {
    function DemoMeetingApp() {
        var _this = this;
        this.cameraDeviceIds = [];
        this.microphoneDeviceIds = [];
        this.meeting = null;
        this.name = null;
        this.region = null;
        this.meetingSession = null;
        this.audioVideo = null;
        this.meetingReadinessChecker = null;
        this.canStartLocalVideo = true;
        this.canHear = null;
        // feature flags
        this.enableWebAudio = false;
        this.enableUnifiedPlanForChromiumBasedBrowsers = false;
        this.enableSimulcast = false;
        this.markdown = require('markdown-it')({ linkify: true });
        this.lastMessageSender = null;
        this.lastReceivedMessageTimestamp = 0;
        this.getAudioInputDevice = function () { return __awaiter(_this, void 0, void 0, function () {
            var audioInputDevices, dropdownList;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.deviceController.listAudioInputDevices()];
                    case 1:
                        audioInputDevices = _a.sent();
                        dropdownList = document.getElementById('audio-input');
                        return [2 /*return*/, this.getDevice(audioInputDevices, dropdownList)];
                }
            });
        }); };
        this.getAudioOutputDevice = function () { return __awaiter(_this, void 0, void 0, function () {
            var audioOutputDevices, dropdownList;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.deviceController.listAudioOutputDevices()];
                    case 1:
                        audioOutputDevices = _a.sent();
                        dropdownList = document.getElementById('audio-output');
                        return [2 /*return*/, this.getDevice(audioOutputDevices, dropdownList)];
                }
            });
        }); };
        this.getVideoInputDevice = function () { return __awaiter(_this, void 0, void 0, function () {
            var videoInputDevices, dropdownList;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.deviceController.listVideoInputDevices()];
                    case 1:
                        videoInputDevices = _a.sent();
                        dropdownList = document.getElementById('video-input');
                        return [2 /*return*/, this.getDevice(videoInputDevices, dropdownList)];
                }
            });
        }); };
        this.getDevice = function (deviceList, dropdownList) { return __awaiter(_this, void 0, void 0, function () {
            var device, i;
            return __generator(this, function (_a) {
                device = deviceList[0];
                for (i = 0; i < deviceList.length; i++) {
                    if (deviceList[i].deviceId === dropdownList.value) {
                        device = deviceList[i];
                    }
                }
                return [2 /*return*/, device];
            });
        }); };
        this.audioTest = function () { return __awaiter(_this, void 0, void 0, function () {
            var audioOutput, speakerUserFeedbackHtml, audioElement, audioOutputResp, textToDisplay;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.createReadinessHtml('speaker-test', 'spinner-border');
                        return [4 /*yield*/, this.getAudioOutputDevice()];
                    case 1:
                        audioOutput = _a.sent();
                        speakerUserFeedbackHtml = document.getElementById('speaker-user-feedback');
                        audioElement = document.getElementById('speaker-test-audio-element');
                        speakerUserFeedbackHtml.style.display = 'inline-block';
                        return [4 /*yield*/, this.meetingReadinessChecker.checkAudioOutput(audioOutput, function () {
                                return new Promise(function (resolve) {
                                    var scheduler = new index_1.IntervalScheduler(1000);
                                    scheduler.start(function () {
                                        if (_this.canHear !== null) {
                                            scheduler.stop();
                                            resolve(_this.canHear);
                                        }
                                    });
                                });
                            }, audioElement)];
                    case 2:
                        audioOutputResp = _a.sent();
                        textToDisplay = index_1.CheckAudioOutputFeedback[audioOutputResp];
                        this.createReadinessHtml('speaker-test', textToDisplay);
                        speakerUserFeedbackHtml.style.display = 'none';
                        return [2 /*return*/, audioOutputResp];
                }
            });
        }); };
        this.micTest = function () { return __awaiter(_this, void 0, void 0, function () {
            var audioInput, audioInputResp;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.createReadinessHtml('mic-test', 'spinner-border');
                        return [4 /*yield*/, this.getAudioInputDevice()];
                    case 1:
                        audioInput = _a.sent();
                        return [4 /*yield*/, this.meetingReadinessChecker.checkAudioInput(audioInput)];
                    case 2:
                        audioInputResp = _a.sent();
                        this.createReadinessHtml('mic-test', index_1.CheckAudioInputFeedback[audioInputResp]);
                        return [2 /*return*/, audioInputResp];
                }
            });
        }); };
        this.videoTest = function () { return __awaiter(_this, void 0, void 0, function () {
            var videoInput, videoInputResp, textToDisplay;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.createReadinessHtml('video-test', 'spinner-border');
                        return [4 /*yield*/, this.getVideoInputDevice()];
                    case 1:
                        videoInput = _a.sent();
                        return [4 /*yield*/, this.meetingReadinessChecker.checkVideoInput(videoInput)];
                    case 2:
                        videoInputResp = _a.sent();
                        textToDisplay = index_1.CheckVideoInputFeedback[videoInputResp];
                        this.createReadinessHtml('video-test', textToDisplay);
                        return [2 /*return*/, videoInputResp];
                }
            });
        }); };
        this.cameraTest = function () { return __awaiter(_this, void 0, void 0, function () {
            var videoInput, cameraResolutionResp1, cameraResolutionResp2, cameraResolutionResp3, textToDisplay;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.createReadinessHtml('camera-test2', 'spinner-border');
                        return [4 /*yield*/, this.getVideoInputDevice()];
                    case 1:
                        videoInput = _a.sent();
                        return [4 /*yield*/, this.meetingReadinessChecker.checkCameraResolution(videoInput, 640, 480)];
                    case 2:
                        cameraResolutionResp1 = _a.sent();
                        return [4 /*yield*/, this.meetingReadinessChecker.checkCameraResolution(videoInput, 1280, 720)];
                    case 3:
                        cameraResolutionResp2 = _a.sent();
                        return [4 /*yield*/, this.meetingReadinessChecker.checkCameraResolution(videoInput, 1920, 1080)];
                    case 4:
                        cameraResolutionResp3 = _a.sent();
                        textToDisplay = index_1.CheckCameraResolutionFeedback[cameraResolutionResp1] + "@640x480p";
                        this.createReadinessHtml('camera-test1', textToDisplay);
                        textToDisplay = index_1.CheckCameraResolutionFeedback[cameraResolutionResp2] + "@1280x720p";
                        this.createReadinessHtml('camera-test2', textToDisplay);
                        textToDisplay = index_1.CheckCameraResolutionFeedback[cameraResolutionResp3] + "@1920x1080p";
                        this.createReadinessHtml('camera-test3', textToDisplay);
                        return [2 /*return*/];
                }
            });
        }); };
        this.contentShareTest = function () { return __awaiter(_this, void 0, void 0, function () {
            var button;
            return __generator(this, function (_a) {
                button = document.getElementById('contentshare-button');
                button.disabled = false;
                return [2 /*return*/];
            });
        }); };
        this.audioConnectivityTest = function () { return __awaiter(_this, void 0, void 0, function () {
            var audioInput, audioConnectivityResp;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.createReadinessHtml('audioconnectivity-test', 'spinner-border');
                        return [4 /*yield*/, this.getAudioInputDevice()];
                    case 1:
                        audioInput = _a.sent();
                        return [4 /*yield*/, this.meetingReadinessChecker.checkAudioConnectivity(audioInput)];
                    case 2:
                        audioConnectivityResp = _a.sent();
                        this.createReadinessHtml('audioconnectivity-test', index_1.CheckAudioConnectivityFeedback[audioConnectivityResp]);
                        return [2 /*return*/, audioConnectivityResp];
                }
            });
        }); };
        this.videoConnectivityTest = function () { return __awaiter(_this, void 0, void 0, function () {
            var videoInput, videoConnectivityResp;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.createReadinessHtml('videoconnectivity-test', 'spinner-border');
                        return [4 /*yield*/, this.getVideoInputDevice()];
                    case 1:
                        videoInput = _a.sent();
                        return [4 /*yield*/, this.meetingReadinessChecker.checkVideoConnectivity(videoInput)];
                    case 2:
                        videoConnectivityResp = _a.sent();
                        this.createReadinessHtml('videoconnectivity-test', index_1.CheckVideoConnectivityFeedback[videoConnectivityResp]);
                        return [2 /*return*/, videoConnectivityResp];
                }
            });
        }); };
        this.networkTcpTest = function () { return __awaiter(_this, void 0, void 0, function () {
            var networkTcpResp;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.createReadinessHtml('networktcp-test', 'spinner-border');
                        return [4 /*yield*/, this.meetingReadinessChecker.checkNetworkTCPConnectivity()];
                    case 1:
                        networkTcpResp = _a.sent();
                        this.createReadinessHtml('networktcp-test', index_1.CheckNetworkTCPConnectivityFeedback[networkTcpResp]);
                        return [2 /*return*/, networkTcpResp];
                }
            });
        }); };
        this.networkUdpTest = function () { return __awaiter(_this, void 0, void 0, function () {
            var networkUdpResp;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.createReadinessHtml('networkudp-test', 'spinner-border');
                        return [4 /*yield*/, this.meetingReadinessChecker.checkNetworkUDPConnectivity()];
                    case 1:
                        networkUdpResp = _a.sent();
                        this.createReadinessHtml('networkudp-test', index_1.CheckNetworkUDPConnectivityFeedback[networkUdpResp]);
                        return [2 /*return*/, networkUdpResp];
                }
            });
        }); };
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        global.app = this;
        document.getElementById('sdk-version-readiness').innerText =
            'amazon-chime-sdk-js@' + index_1.Versioning.sdkVersion;
        this.initEventListeners();
        this.initParameters();
        this.setMediaRegion();
        this.switchToFlow('flow-authenticate');
    }
    DemoMeetingApp.prototype.switchToFlow = function (flow) {
        this.analyserNodeCallback = function () { };
        Array.from(document.getElementsByClassName('flow')).map(function (e) { return (e.style.display = 'none'); });
        document.getElementById(flow).style.display = 'block';
    };
    DemoMeetingApp.prototype.initParameters = function () {
        var _this = this;
        this.defaultBrowserBehaviour = new index_1.DefaultBrowserBehavior();
        // Initialize logger and device controller to populate device list
        new index_1.AsyncScheduler().start(function () { return __awaiter(_this, void 0, void 0, function () {
            var button;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.initializeLoggerAndDeviceController()];
                    case 1:
                        _a.sent();
                        button = document.getElementById("authenticate");
                        button.disabled = false;
                        return [2 /*return*/];
                }
            });
        }); });
    };
    DemoMeetingApp.prototype.startMeetingAndInitializeMeetingReadinessChecker = function () {
        return __awaiter(this, void 0, void 0, function () {
            var chimeMeetingId, error_1, httpErrorMessage;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        chimeMeetingId = '';
                        this.meeting = "READINESS_CHECKER-" + uuidv4();
                        this.name = "READINESS_CHECKER" + uuidv4();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        this.region = document.getElementById('inputRegion').value;
                        return [4 /*yield*/, this.authenticate()];
                    case 2:
                        chimeMeetingId = _a.sent();
                        this.log("chimeMeetingId: " + chimeMeetingId);
                        return [2 /*return*/, chimeMeetingId];
                    case 3:
                        error_1 = _a.sent();
                        httpErrorMessage = 'UserMedia is not allowed in HTTP sites. Either use HTTPS or enable media capture on insecure sites.';
                        document.getElementById('failed-meeting').innerText = "Meeting ID: " + this.meeting;
                        document.getElementById('failed-meeting-error').innerText =
                            window.location.protocol === 'http:' ? httpErrorMessage : error_1.message;
                        this.switchToFlow('flow-failed-meeting');
                        return [2 /*return*/, null];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    DemoMeetingApp.prototype.authenticate = function () {
        return __awaiter(this, void 0, void 0, function () {
            var joinInfo, configuration;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.joinMeeting()];
                    case 1:
                        joinInfo = (_a.sent()).JoinInfo;
                        configuration = new index_1.MeetingSessionConfiguration(joinInfo.Meeting, joinInfo.Attendee);
                        return [4 /*yield*/, this.initializeMeetingSession(configuration)];
                    case 2:
                        _a.sent();
                        return [2 /*return*/, configuration.meetingId];
                }
            });
        });
    };
    DemoMeetingApp.prototype.createLogStream = function (configuration) {
        return __awaiter(this, void 0, void 0, function () {
            var body, response, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        body = JSON.stringify({
                            meetingId: configuration.meetingId,
                            attendeeId: configuration.credentials.attendeeId,
                        });
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, fetch(DemoMeetingApp.BASE_URL + "create_log_stream", {
                                method: 'POST',
                                body: body
                            })];
                    case 2:
                        response = _a.sent();
                        if (response.status === 200) {
                            console.log('Log stream created');
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        error_2 = _a.sent();
                        console.error(error_2.message);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    DemoMeetingApp.prototype.createReadinessHtml = function (id, textToDisplay) {
        var readinessElement = document.getElementById(id);
        readinessElement.innerHTML = '';
        readinessElement.innerText = textToDisplay;
        if (id === 'readiness-header') {
            return;
        }
        else if (textToDisplay === 'spinner-border') {
            readinessElement.innerHTML = '';
            readinessElement.className = '';
            readinessElement.className = 'spinner-border';
        }
        else if (textToDisplay.includes('Succeeded')) {
            readinessElement.className = '';
            readinessElement.className = 'badge badge-success';
        }
        else {
            readinessElement.className = 'badge badge-warning';
        }
    };
    DemoMeetingApp.prototype.initEventListeners = function () {
        var _this = this;
        //event listener for user feedback for speaker output
        document.getElementById('speaker-yes').addEventListener('input', function (e) {
            e.preventDefault();
            _this.canHear = true;
        });
        document.getElementById('speaker-no').addEventListener('input', function (e) {
            e.preventDefault();
            _this.canHear = false;
        });
        var contentShareButton = document.getElementById('contentshare-button');
        contentShareButton.addEventListener('click', function () { return __awaiter(_this, void 0, void 0, function () {
            var contentShareResult, contentShareResp;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        contentShareButton.style.display = 'none';
                        contentShareResult = document.getElementById('contentshare-test');
                        contentShareResult.style.display = 'inline-block';
                        this.createReadinessHtml('contentshare-test', 'spinner-border');
                        return [4 /*yield*/, this.meetingReadinessChecker.checkContentShareConnectivity()];
                    case 1:
                        contentShareResp = _a.sent();
                        this.createReadinessHtml('contentshare-test', index_1.CheckContentShareConnectivityFeedback[contentShareResp]);
                        contentShareButton.disabled = true;
                        return [2 /*return*/];
                }
            });
        }); });
        document.getElementById('form-authenticate').addEventListener('submit', function (e) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        e.preventDefault();
                        return [4 /*yield*/, this.startMeetingAndInitializeMeetingReadinessChecker()];
                    case 1:
                        if (!!!(_a.sent())) return [3 /*break*/, 11];
                        this.switchToFlow('flow-readinesstest');
                        //create new HTML header
                        document.getElementById('sdk-version').innerText =
                            'amazon-chime-sdk-js@' + index_1.Versioning.sdkVersion;
                        this.createReadinessHtml('readiness-header', 'Readiness tests underway...');
                        return [4 /*yield*/, this.audioTest()];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, this.micTest()];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, this.videoTest()];
                    case 4:
                        _a.sent();
                        return [4 /*yield*/, this.cameraTest()];
                    case 5:
                        _a.sent();
                        return [4 /*yield*/, this.networkUdpTest()];
                    case 6:
                        _a.sent();
                        return [4 /*yield*/, this.networkTcpTest()];
                    case 7:
                        _a.sent();
                        return [4 /*yield*/, this.audioConnectivityTest()];
                    case 8:
                        _a.sent();
                        return [4 /*yield*/, this.videoConnectivityTest()];
                    case 9:
                        _a.sent();
                        return [4 /*yield*/, this.contentShareTest()];
                    case 10:
                        _a.sent();
                        this.createReadinessHtml('readiness-header', 'Readiness tests complete!');
                        _a.label = 11;
                    case 11: return [2 /*return*/];
                }
            });
        }); });
    };
    DemoMeetingApp.prototype.initializeLoggerAndDeviceController = function (configuration) {
        return __awaiter(this, void 0, void 0, function () {
            var logger, logLevel, consoleLogger;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        logLevel = index_1.LogLevel.INFO;
                        consoleLogger = (logger = new index_1.ConsoleLogger('SDK', logLevel));
                        if (!((location.hostname === 'localhost' || location.hostname === '127.0.0.1') || configuration == null)) return [3 /*break*/, 1];
                        this.logger = consoleLogger;
                        return [3 /*break*/, 3];
                    case 1: return [4 /*yield*/, this.createLogStream(configuration)];
                    case 2:
                        _a.sent();
                        this.logger = new index_1.MultiLogger(consoleLogger, new index_1.MeetingSessionPOSTLogger('SDK', configuration, DemoMeetingApp.LOGGER_BATCH_SIZE, DemoMeetingApp.LOGGER_INTERVAL_MS, DemoMeetingApp.BASE_URL + "logs", logLevel));
                        _a.label = 3;
                    case 3:
                        this.deviceController = new index_1.DefaultDeviceController(logger);
                        return [4 /*yield*/, this.populateAllDeviceLists()];
                    case 4:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    DemoMeetingApp.prototype.initializeMeetingSession = function (configuration) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.initializeLoggerAndDeviceController(configuration)];
                    case 1:
                        _a.sent();
                        configuration.enableWebAudio = this.enableWebAudio;
                        configuration.enableUnifiedPlanForChromiumBasedBrowsers = this.enableUnifiedPlanForChromiumBasedBrowsers;
                        configuration.attendeePresenceTimeoutMs = 15000;
                        configuration.enableSimulcastForUnifiedPlanChromiumBasedBrowsers = this.enableSimulcast;
                        this.meetingSession = new index_1.DefaultMeetingSession(configuration, this.logger, this.deviceController);
                        this.audioVideo = this.meetingSession.audioVideo;
                        this.meetingReadinessChecker = new index_1.DefaultMeetingReadinessChecker(this.logger, this.meetingSession);
                        // eslint-disable-next-line @typescript-eslint/no-explicit-any
                        global.meetingReadinessChecker = this.meetingReadinessChecker;
                        this.setupDeviceLabelTrigger();
                        return [2 /*return*/];
                }
            });
        });
    };
    DemoMeetingApp.prototype.setupDeviceLabelTrigger = function () {
        var _this = this;
        // Note that device labels are privileged since they add to the
        // fingerprinting surface area of the browser session. In Chrome private
        // tabs and in all Firefox tabs, the labels can only be read once a
        // MediaStream is active. How to deal with this restriction depends on the
        // desired UX. The device controller includes an injectable device label
        // trigger which allows you to perform custom behavior in case there are no
        // labels, such as creating a temporary audio/video stream to unlock the
        // device names, which is the default behavior. Here we override the
        // trigger to also show an alert to let the user know that we are asking for
        // mic/camera permission.
        //
        // Also note that Firefox has its own device picker, which may be useful
        // for the first device selection. Subsequent device selections could use
        // a custom UX with a specific device id.
        this.audioVideo.setDeviceLabelTrigger(function () { return __awaiter(_this, void 0, void 0, function () {
            var stream;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, navigator.mediaDevices.getUserMedia({ audio: true, video: true })];
                    case 1:
                        stream = _a.sent();
                        return [2 /*return*/, stream];
                }
            });
        }); });
    };
    DemoMeetingApp.prototype.populateAllDeviceLists = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.populateAudioInputList()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.populateVideoInputList()];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, this.populateAudioOutputList()];
                    case 3:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    DemoMeetingApp.prototype.populateAudioInputList = function () {
        return __awaiter(this, void 0, void 0, function () {
            var genericName, additionalDevices, _a, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        genericName = 'Microphone';
                        additionalDevices = ['None'];
                        _a = this.populateDeviceList;
                        _b = ['audio-input',
                            genericName];
                        return [4 /*yield*/, this.deviceController.listAudioInputDevices()];
                    case 1:
                        _a.apply(this, _b.concat([_c.sent(), additionalDevices]));
                        return [2 /*return*/];
                }
            });
        });
    };
    DemoMeetingApp.prototype.populateVideoInputList = function () {
        return __awaiter(this, void 0, void 0, function () {
            var genericName, additionalDevices, _a, _b, cameras;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        genericName = 'Camera';
                        additionalDevices = ['None'];
                        _a = this.populateDeviceList;
                        _b = ['video-input',
                            genericName];
                        return [4 /*yield*/, this.deviceController.listVideoInputDevices()];
                    case 1:
                        _a.apply(this, _b.concat([_c.sent(), additionalDevices]));
                        return [4 /*yield*/, this.deviceController.listVideoInputDevices()];
                    case 2:
                        cameras = _c.sent();
                        this.cameraDeviceIds = cameras.map(function (deviceInfo) {
                            return deviceInfo.deviceId;
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    DemoMeetingApp.prototype.populateAudioOutputList = function () {
        return __awaiter(this, void 0, void 0, function () {
            var genericName, additionalDevices, _a, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        genericName = 'Speaker';
                        additionalDevices = [];
                        _a = this.populateDeviceList;
                        _b = ['audio-output',
                            genericName];
                        return [4 /*yield*/, this.deviceController.listAudioOutputDevices()];
                    case 1:
                        _a.apply(this, _b.concat([_c.sent(), additionalDevices]));
                        return [2 /*return*/];
                }
            });
        });
    };
    DemoMeetingApp.prototype.populateDeviceList = function (elementId, genericName, devices, additionalOptions) {
        var e_1, _a;
        var list = document.getElementById(elementId);
        while (list.firstElementChild) {
            list.removeChild(list.firstElementChild);
        }
        for (var i = 0; i < devices.length; i++) {
            var option = document.createElement('option');
            list.appendChild(option);
            option.text = devices[i].label || genericName + " " + (i + 1);
            option.value = devices[i].deviceId;
        }
        if (additionalOptions.length > 0) {
            var separator = document.createElement('option');
            separator.disabled = true;
            separator.text = '──────────';
            list.appendChild(separator);
            try {
                for (var additionalOptions_1 = __values(additionalOptions), additionalOptions_1_1 = additionalOptions_1.next(); !additionalOptions_1_1.done; additionalOptions_1_1 = additionalOptions_1.next()) {
                    var additionalOption = additionalOptions_1_1.value;
                    var option = document.createElement('option');
                    list.appendChild(option);
                    option.text = additionalOption;
                    option.value = additionalOption;
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (additionalOptions_1_1 && !additionalOptions_1_1.done && (_a = additionalOptions_1.return)) _a.call(additionalOptions_1);
                }
                finally { if (e_1) throw e_1.error; }
            }
        }
        if (!list.firstElementChild) {
            var option = document.createElement('option');
            option.text = 'Device selection unavailable';
            list.appendChild(option);
        }
    };
    DemoMeetingApp.prototype.join = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                window.addEventListener('unhandledrejection', function (event) {
                    _this.log(event.reason);
                });
                this.audioVideo.start();
                return [2 /*return*/];
            });
        });
    };
    // eslint-disable-next-line
    DemoMeetingApp.prototype.joinMeeting = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, json;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, fetch(DemoMeetingApp.BASE_URL + "join?title=" + encodeURIComponent(this.meeting) + "&name=" + encodeURIComponent(this.name) + "&region=" + encodeURIComponent(this.region), {
                            method: 'POST',
                        })];
                    case 1:
                        response = _a.sent();
                        return [4 /*yield*/, response.json()];
                    case 2:
                        json = _a.sent();
                        if (json.error) {
                            throw new Error("Server error: " + json.error);
                        }
                        return [2 /*return*/, json];
                }
            });
        });
    };
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    DemoMeetingApp.prototype.endMeeting = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, fetch(DemoMeetingApp.BASE_URL + "end?title=" + encodeURIComponent(this.meeting), {
                            method: 'POST',
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    DemoMeetingApp.prototype.getSupportedMediaRegions = function () {
        var supportedMediaRegions = [];
        var mediaRegion = document.getElementById('inputRegion');
        for (var i = 0; i < mediaRegion.length; i++) {
            supportedMediaRegions.push(mediaRegion.value);
        }
        return supportedMediaRegions;
    };
    DemoMeetingApp.prototype.getNearestMediaRegion = function () {
        return __awaiter(this, void 0, void 0, function () {
            var nearestMediaRegionResponse, nearestMediaRegionJSON, nearestMediaRegion;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, fetch("https://nearest-media-region.l.chime.aws", {
                            method: 'GET',
                        })];
                    case 1:
                        nearestMediaRegionResponse = _a.sent();
                        return [4 /*yield*/, nearestMediaRegionResponse.json()];
                    case 2:
                        nearestMediaRegionJSON = _a.sent();
                        nearestMediaRegion = nearestMediaRegionJSON.region;
                        return [2 /*return*/, nearestMediaRegion];
                }
            });
        });
    };
    DemoMeetingApp.prototype.setMediaRegion = function () {
        var _this = this;
        new index_1.AsyncScheduler().start(function () { return __awaiter(_this, void 0, void 0, function () {
            var nearestMediaRegion, supportedMediaRegions, mediaRegionElement, newMediaRegionOption, error_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.getNearestMediaRegion()];
                    case 1:
                        nearestMediaRegion = _a.sent();
                        if (nearestMediaRegion === '' || nearestMediaRegion === null) {
                            throw new Error('Nearest Media Region cannot be null or empty');
                        }
                        supportedMediaRegions = this.getSupportedMediaRegions();
                        if (supportedMediaRegions.indexOf(nearestMediaRegion) === -1) {
                            supportedMediaRegions.push(nearestMediaRegion);
                            mediaRegionElement = document.getElementById('inputRegion');
                            newMediaRegionOption = document.createElement('option');
                            newMediaRegionOption.value = nearestMediaRegion;
                            newMediaRegionOption.text = nearestMediaRegion + ' (' + nearestMediaRegion + ')';
                            mediaRegionElement.add(newMediaRegionOption, null);
                        }
                        document.getElementById('inputRegion').value = nearestMediaRegion;
                        return [3 /*break*/, 3];
                    case 2:
                        error_3 = _a.sent();
                        this.log('Default media region selected: ' + error_3.message);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        }); });
    };
    DemoMeetingApp.prototype.log = function (str) {
        console.log("[Meeting Readiness Checker] " + str);
    };
    DemoMeetingApp.BASE_URL = [
        location.protocol,
        '//',
        location.host,
        location.pathname.replace(/\/*$/, '/'),
    ].join('');
    DemoMeetingApp.LOGGER_BATCH_SIZE = 85;
    DemoMeetingApp.LOGGER_INTERVAL_MS = 2000;
    return DemoMeetingApp;
}());
exports.DemoMeetingApp = DemoMeetingApp;
window.addEventListener('load', function () {
    new DemoMeetingApp();
});
//# sourceMappingURL=meetingReadinessChecker.js.map